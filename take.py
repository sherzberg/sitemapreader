import sys
import itertools

url = sys.argv[2]
count = int(sys.argv[1])


from sitemapreader import get_sitemap_urls

for _ in itertools.islice(get_sitemap_urls(url), count):
    print _
