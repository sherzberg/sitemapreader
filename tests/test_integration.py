from unittest import TestCase, skip

from sitemapreader import get_sitemap_urls


def greater_than(gen, count):
    for i, _ in enumerate(gen):
        if i > count:
            break

    else:
        raise Exception('{} is not greater than {}'.format(i, count))


class IntegrationTest(TestCase):

    def test_officedepot(self):
        gen = get_sitemap_urls('http://www.officedepot.com/robots.txt')

        greater_than(gen, 10000)

    def test_newegg(self):
        gen = get_sitemap_urls('http://www.newegg.com/robots.txt')

        greater_than(gen, 10000)

    def test_target(self):
        gen = get_sitemap_urls('http://www.target.com/robots.txt')

        greater_than(gen, 10000)

    @skip
    def test_amazon(self):
        gen = get_sitemap_urls('http://www.amazon.com/robots.txt')

        greater_than(gen, 10000)

    def test_bestbuy(self):
        gen = get_sitemap_urls('http://www.bestbuy.com/robots.txt')

        greater_than(gen, 10000)

    def test_homedepot(self):
        gen = get_sitemap_urls('http://www.homedepot.com/robots.txt')

        greater_than(gen, 10000)

    def test_walmart(self):
        gen = get_sitemap_urls('http://www.walmart.com/robots.txt')

        greater_than(gen, 10000)

