import logging
import re
import gzip
import urllib2
from gzip import GzipFile
from cStringIO import StringIO

logger = logging.getLogger(__name__)


SITEMAP_RE = re.compile('''http.*\/\/.*(?:\.xml|\.gz|robots.txt)''')
URL_RE = re.compile('''(http.*\/\/.*)(?:<|\W|\s)+''')


def get_sitemap_urls(url, headers=None, silent=False):
    logger.debug("parsing {}".format(url))

    try:
        opener = urllib2.build_opener()
        if headers:
            opener.addheaders = headers
        u = opener.open(url)
    except urllib2.HTTPError as e:
        if e.code == 404:
            logger.debug('{} is a 404'.format(url))
            return
        else:
            if silent:
                logger.debug('{} is: %d'.format(url) % e.code)
                return
            else:
                raise e

    if 'content-type' in u.headers and u.headers["content-type"].lower() == "application/x-gzip":
        content = GzipFile(fileobj=StringIO(u.read())).read()
    elif 'content-type' in u.headers and u.headers["content-type"].lower() == "application/octet-stream":
        with open('/tmp/test.gz', 'wb') as fh:
            fh.write(u.read())

        with gzip.open('/tmp/test.gz', 'rb') as fh:
            content = fh.read()
    elif 'content-type' in u.headers and u.headers["content-type"].lower() == "application/xml" and 'content-encoding' in u.headers and u.headers['content-encoding'].lower() == 'x-gzip':
        with open('/tmp/test.gz', 'wb') as fh:
            fh.write(u.read())

        with gzip.open('/tmp/test.gz', 'rb') as fh:
            content = fh.read()
    else:
        content = u.read()

    content = '\n'.join(re.split('<', content))

    for match in re.finditer(SITEMAP_RE, content):
        sitemap_url = match.group()

        for _url in get_sitemap_urls(sitemap_url, headers, silent):
            yield _url

    else:
        for match in re.finditer(URL_RE, content):
            site_url = re.split('\s', match.group().split('<')[0])[0]
            site_url = site_url.replace(']]>', '')

            not_image = not site_url.endswith('jpg')

            # TODO: catch more edge cases...
            if 'sitemaps.org' not in site_url and not_image:
                yield site_url

