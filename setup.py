from setuptools import find_packages
from setuptools import setup


setup(
    name='sitemapreader',
    description='sitemapreader',
    url='https://github.com/pre-commit/pre-commit-hooks',
    version='0.0.1',

    author='Bawte',
    author_email='dev@bawte.com',

    platforms='linux',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],

    packages=find_packages('.', exclude=('tests*', 'testing*')),
    install_requires=[]
)
